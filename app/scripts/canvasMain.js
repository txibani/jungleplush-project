/**
 * Created by maitegracia on 13/03/15.
 */


//http://kevs3d.co.uk/dev/eglogo/
window.addEventListener('load', onloadHandler, false);

function onloadHandler()
{
    // get the canvas DOM element
    var canvas = document.getElementById('canvas'),
        ctx = canvas.getContext("2d"),
        width = canvas.width,
        height = canvas.height;

    // data structures - generate initial blobs
    var blobList = new BlobList();
    blobList.blobs = [
        new Blob(150, 160, 0, 30, "rgba(220,5,134,.9)"), //pink
        new Blob(67,  336, 0, 30, "rgba(145,200,46,.9)"), //green
        new Blob(406, 374,  0, 30, "rgba(45,172,173,.9)"), //soft orange
        new Blob(514, 52,  0, 30, "rgba(238,172,78,.75)"), //soft orange
        new Blob(220, 82,  0, 30, "rgba(47,118,178,.9)"), //aguamarina
        new Blob(350, 100, 0, 30, "rgba(248,172,78,.9)"), //orange
        new Blob(232, 332, 0, 30, "rgba(99,198,78,.9)"), // green chillon
        new Blob(556, 222, 0, 30, "rgba(140,45,136,.9)"), // purple
        new Blob(128, 44, 0, 30, "rgba(226,74,63,.9)"), // red
        new Blob(264,  188, 0, 30, "rgba(255,203,78,.9)") //yellow
    ];

    // event handlers
    var mouseMove = function(e)
    {
        blobList.mousex = e.clientX;
        blobList.mousey = e.clientY;
    };
    canvas.addEventListener("mousemove", mouseMove, false);

    var offsetx = 0, offsety = 0;

    // init main animation loop
    requestAnimFrame(loop);
    function loop()
    {
        // compute canvas offset within parent window including page view position
        var el = canvas;
        offsetx = offsety = 0;
        do
        {
            offsetx += el.offsetLeft;
            offsety += el.offsetTop;
        } while (el = el.offsetParent);
        offsetx = offsetx - window.pageXOffset;
        offsety = offsety - window.pageYOffset;

        ctx.save();

        // clear the left side of the card
        // the right hand side is already rendered with fixed text
        ctx.clearRect(0, 0, width, height);


        // render each edge blob - which react to mouse movement
        ctx.globalCompositeOperation = 'darker';
        blobList.update();
        blobList.render();

        ctx.restore();

        requestAnimFrame(loop);
    };

    // data structures
    function BlobList()
    {
        this.mousex = this.mousey = 0;
        this.blobs = [];

        this.update = function()
        {
            // special case for first blob - which is the main magenta disc
            var blob = this.blobs[0];
            if (Rnd() > 0.99)
            {
                blob.velocity.z += (Rnd()*0.10 - 0.05);
                blob.spring = 0.0125;
            }
            blob.update();

            // all the other blobs can animate based on mouse interaction
            for (var i = 1,dx,dy,d; i < this.blobs.length; i++)
            {
                blob = this.blobs[i];

                // calculate offset from mouse position - apply canvas element offset
                dx = this.mousex - (blob.position.x + offsetx);
                dy = this.mousey - (blob.position.y + offsety);
                d = Sqrt(dx * dx + dy * dy);

                // if the mouse is within the radius of a blog - then nudge it out
                var rad = (blob.originradius > 16 ? blob.originradius : 16);
                if (d < rad)
                {
                    blob.targetPos.x = blob.position.x - dx;
                    blob.targetPos.y = blob.position.y - dy;
                    blob.spring = 0.033;
                }
                // else based on a random chance, pulse the blob
                else if (Rnd() > 0.995)
                {
                    blob.targetPos.x = blob.origin.x;
                    blob.targetPos.y = blob.origin.y;
                    blob.velocity.z += (Rnd()*0.30 - 0.15);
                    blob.spring = 0.0125;
                }
                // else just animate towards the original position
                else
                {
                    blob.targetPos.x = blob.origin.x;
                    blob.targetPos.y = blob.origin.y;
                    blob.spring = 0.05;
                }

                blob.update();
            }
        };

        this.render = function()
        {
            for (var i = 0; i < this.blobs.length; i++)
            {
                ctx.save();
                this.blobs[i].render();
                ctx.restore();
            }
        };
    };

    function Blob(x, y, z, radius, colour)
    {
        this.origin = new Vector3D(x,y,z);
        this.position = new Vector3D(x,y,z);
        this.targetPos = new Vector3D(x,y,z);
        this.originradius = radius;
        this.radius = radius;
        this.velocity = new Vector3D(0,0,0);
        this.colour = colour;
        this.friction = 0.75;
        this.spring = 0.05;

        this.update = function()
        {
            this.velocity.x += (this.targetPos.x - this.position.x) * this.spring;
            this.velocity.x *= this.friction;
            this.position.x += this.velocity.x;

            this.velocity.y += (this.targetPos.y - this.position.y) * this.spring;
            this.velocity.y *= this.friction;
            this.position.y += this.velocity.y;

            var dox = this.origin.x - this.position.x,
                doy = this.origin.y - this.position.y,
                d = Sqrt(dox * dox + doy * doy);

            this.targetPos.z = d/150 + 1;
            this.velocity.z += (this.targetPos.z - this.position.z) * this.spring;
            this.velocity.z *= this.friction;
            this.position.z += this.velocity.z;

            this.radius = this.originradius * this.position.z;
            if (this.radius < 1) this.radius = 1;
        };

        this.render = function()
        {
            ctx.fillStyle = this.colour;
            ctx.beginPath();
            ctx.arc(this.position.x, this.position.y, this.radius, 0, TWOPI, true);
            ctx.fill();
        };
    };
}

// requestAnimFrame shim
window.requestAnimFrame = (function()
{
    return  window.requestAnimationFrame       ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame    ||
        window.oRequestAnimationFrame      ||
        window.msRequestAnimationFrame     ||
        function(callback, element)
        {
            window.setTimeout(callback, 1000 / 60);
        };
})();