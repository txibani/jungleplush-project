
// Main Application module

var app = angular.module('app', []);

/**
 * Main homepage controller
 */
app.controller('MainController', ['$scope', MainController]);

function MainController($scope) {

    // Language detection base in navigator language
   $scope.language = window.navigator.userLanguage || window.navigator.language;
    console.log($scope.language);
}
