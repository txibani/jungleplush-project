// - GLOBAL VARIABLES


// Contact us validation form
var formComment = $('#newComment');

function validateForm() {
    'use strict';
    formComment.validate({
        rules: {
            secondName: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                email: true
            },
            telephone: {
                required: false,
                number: true
            }
        }
    });
}


// Contact us submit form
function submitForm() {
    'use strict';
    formComment.on('submit', function(e) {
        e.preventDefault();
        var _this = $(this);

        var data = {
            firstName: _this.find('#firstName').val(),
            secondName: _this.find('#secondName').val(),
            email: _this.find('#email').val(),
            phone: _this.find('#telephone').val(),
            comment: _this.find('#comments').val()
        };

        if (_this.valid()) {
            $.ajax({
                type: 'POST',
                url: 'php/proces.php', //process to mail
                data: data,
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                success: function() {
                    formComment.find('input').val('');
                    formComment.find('textarea').val('');
                    console.log('Your message has been sent!!');
                },
                error: function() {
                    console.log('failure');
                }
            });
        }
    });
}


// Show/Hide Dino for Small devices
function showHideDino() {
    'use strict';
    if ($(window).width() <= 767) {
        $('.dinoSiluete').click(function() {
            var _this = $(this);
            _this.parent().find('.displayDinoInfo').fadeToggle();
        });
    }
}

function videoController() {
  var vid = document.getElementById("bgvid");
  var pauseButton = document.getElementById("vidpause");
  function vidFade() {
    vid.classList.add("stopfade");
  }
  vid.addEventListener('ended', function() {
// only functional if "loop" is removed
    vid.pause();
// to capture IE10
    vidFade();
  });
  pauseButton.addEventListener("click", function() {
    vid.classList.toggle("stopfade");
    if (vid.paused) {
      vid.play();
      pauseButton.innerHTML = "Pause";
    } else {
      vid.pause();
      pauseButton.innerHTML = "Paused";
    }
  })
};

// Function conservativeCollapse
$(document).ready(function() {
    'use strict';
    $(window).on('beforeunload', function() {
        $(window).scrollTop(0);
    });
    validateForm();
    submitForm();
    showHideDino();
    videoController();
});
