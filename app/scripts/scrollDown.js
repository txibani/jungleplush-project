/**
 * Created by maitegracia on 11/03/15.
 */

// - GLOBAL VARIABLES

var movingBtn = $('.theme__button'),
    btnIF = movingBtn.find('i');
function btnMove() {

    'use strict';
    var NUM_SLIDES = 6;

    var pageMove = $('html, body');

    movingBtn.on('click', function() {
        var _this = $(this), visible = +_this.attr('data-visible'), nextIndex = visible + 1;
        var currentSection = $('.section[data-index="'+ nextIndex  +'"]');


        if (nextIndex >= NUM_SLIDES) {
            nextIndex = 1;
            pageMove.animate({
                scrollTop: $('.section1').offset().top
            }, 1000, function(){
                _this.attr('data-visible', nextIndex);
            });
        } else {
            console.log('Im in section: ' + currentSection);
            pageMove.animate({
                scrollTop: $(currentSection).offset().top
            }, 1000, function(){
                _this.attr('data-visible', nextIndex);
            });
        }

        if (nextIndex === 1){
            movingBtn.css('background-color', '#FFEB3B');
            if (btnIF.hasClass('fa-arrow-up')) {
                btnIF.removeClass('fa-arrow-up');
                btnIF.addClass('fa-arrow-down');
            }
        } else if (nextIndex === 2){
            movingBtn.css('background-color' ,'#FF4081');
        } else if (nextIndex === 3) {
            movingBtn.css('background-color' ,'#9C27B0');
        } else if (nextIndex === 4) {
            movingBtn.css('background-color' ,'#FF5252');
        } else if (nextIndex === 5) {
            movingBtn.css('background-color', '#3F51B5');
             if (btnIF.hasClass('fa-arrow-down')) {
                btnIF.removeClass('fa-arrow-down');
                btnIF.addClass('fa-arrow-up');
            }

        }
    });
}

function scrollDownd() {
    'use strict';

    var windowHeight = $(window).height();
    var scrol_pos = 0;

    $(window).scroll(function() {
        scrol_pos = $(this).scrollTop();
        if ((scrol_pos > windowHeight-400) && (scrol_pos < (windowHeight*2-400))) {
            movingBtn.css('background-color' ,'#FF4081');
            $('.indicationList li:nth-child(1) > a').removeClass('currentPage');
            $('.indicationList li:nth-child(2) > a').addClass('currentPage');
            $('.indicationList li:nth-child(3) > a').removeClass('currentPage');
            movingBtn.attr('data-visible', 2);

        } else if ((scrol_pos > (windowHeight*2-400)) && (scrol_pos < (windowHeight*3-400)))  {
            movingBtn.css('background-color' ,'#9C27B0');
            $('.indicationList li:nth-child(2) > a').removeClass('currentPage');
            $('.indicationList li:nth-child(3) > a').addClass('currentPage');
            $('.indicationList li:nth-child(4) > a').removeClass('currentPage');
            movingBtn.attr('data-visible', 3);

        } else if ((scrol_pos > (windowHeight*3-400)) && (scrol_pos < (windowHeight*4-400)))  {
            movingBtn.css('background-color' ,'#FF5252');
            $('.indicationList li:nth-child(3) > a').removeClass('currentPage');
            $('.indicationList li:nth-child(4) > a').addClass('currentPage');
            $('.indicationList li:nth-child(5) > a').removeClass('currentPage');
            movingBtn.attr('data-visible', 4);

        } else if ((scrol_pos > (windowHeight*4-400)) && (scrol_pos < (windowHeight*5-400)))  {
            movingBtn.css('background-color', '#3F51B5');
            $('.indicationList li:nth-child(4) > a').removeClass('currentPage');
            $('.indicationList li:nth-child(5) > a').addClass('currentPage');
            movingBtn.attr('data-visible',5);
            if (btnIF.hasClass('fa-arrow-down')) {
                btnIF.removeClass('fa-arrow-down');
                btnIF.addClass('fa-arrow-up');
            }

        } else if (scrol_pos < windowHeight){
            movingBtn.css('background-color', '#FFEB3B');
            $('.indicationList li > a').removeClass('currentPage');
            $('.indicationList li:nth-child(1) > a').addClass('currentPage');
            movingBtn.attr('data-visible', 1);
            if (btnIF.hasClass('fa-arrow-up')) {
                btnIF.removeClass('fa-arrow-up');
                btnIF.addClass('fa-arrow-down');
            }
        }

    });
}


$(document).ready(function() {
    'use strict';
    btnMove();
    scrollDownd();
});